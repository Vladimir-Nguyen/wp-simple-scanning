import urllib2
from BeautifulSoup import BeautifulSoup
from prettytable import PrettyTable
import re


def getSource(url):
	res = urllib2.urlopen(url)
	content = res.read()
	return content

def getVer(content):
	r_table = PrettyTable(['Name', 'Version'])

	versions = content.findAll("meta", {"name": "generator"})
	for ver in versions:
		ver_wp = re.findall('\w+ \d+\.\d+\.\d+', str(ver))
		ver_wpml = re.findall('WPML ver:\d+.\d+.\d+', str(ver))

		if ver_wp:
			result_wp = ver_wp[0].split(" ")
			if (result_wp[0] == "WordPress"):
				r_table.add_row([result_wp[0], result_wp[1]])
		elif ver_wpml:
			result_wpml = ver_wpml[0].split(" ")
			result_wpml[1] = result_wpml[1].replace('ver:', '')
			r_table.add_row([result_wpml[0], result_wpml[1]])
	print r_table

def getPlugin(content):
	t_plugin = PrettyTable(['Plugin Name', 'Version'])
	plugins = content.findAll("script", {"type": "text/javascript"})
	for plugin in plugins:

		plugin = re.findall('((\w+\.){1,5}\w+\?ver=(\d+\.){2,3}\d)', str(plugin))
		if plugin:
			result = plugin[0][0].split("?")
			result[1] = result[1].replace('ver=', '')
			t_plugin.add_row([result[0], result[1]])
	print t_plugin

def getUsers(content):
	t_user = PrettyTable(['Username'])
	users = re.findall('slug":"\w+', content)
	for user in users:
		user = user.split('":"')
		t_user.add_row([user[1]])
	print t_user

			
def filterHTML(content):
	html_string = content
	parsed_html = BeautifulSoup(html_string)
	
	getVer(parsed_html)
	getPlugin(parsed_html)



with open('urls.txt') as urls:
	for url in urls:
		print "\n[+] Checking sites " + url
		content = getSource(url)
		filterHTML(content)
		# Enumerate user
		users = getSource(url.strip() + "/wp-json/wp/v2/users")
		getUsers(users)
		#reportField()
		
